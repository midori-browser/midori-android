package org.midorinext.android.tabs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import kotlinx.android.synthetic.main.activity_tab.*
import org.midorinext.android.TabActivity
import kotlinx.android.synthetic.main.view_tab.view.*
import org.midorinext.android.WebView

object TabInfo {
    var normalTabs = mutableListOf<Tab>()
    var incognitoTabs = mutableListOf<Tab>()
    var currentIndex = -1

    lateinit var activity: TabActivity

    fun count() = if (activity.addMode() == "NORMAL") normalTabs.size else incognitoTabs.size

    fun addTab(address: String, mMode: String?) {
        val tab = Tab(activity)
        tab.web_view.loadUrl(address)
        addTab(tab, mMode)
    }

    fun addTab(tab: Tab, mMode: String?) {
        if (mMode == "NORMAL") {
            currentIndex = normalTabs.size
            normalTabs.add(tab)
        } else {
            currentIndex = incognitoTabs.size
            incognitoTabs.add(tab)
        }
    }

    fun removeTab(index: Int) {
        if (activity.addMode() == "NORMAL") {
            val tab = normalTabs[index]
            tab.web_view.destroy()

            normalTabs.removeAt(index)
            currentIndex--
            //if (currentIndex < 0) currentIndex = normalTabs.size - 1
        }else{
            val tab = incognitoTabs[index]
            tab.web_view.destroy()

            incognitoTabs.removeAt(index)
            currentIndex--
            if (currentIndex < 0) currentIndex = incognitoTabs.size - 1
        }
    }

    fun removeAllTabs() {
        if (activity.addMode() == "NORMAL") {
            for (tab in normalTabs) tab.web_view.destroy()
            normalTabs.clear()
            currentIndex = 0
        }else{
            for (tab in incognitoTabs) tab.web_view.destroy()
            incognitoTabs.clear()
            currentIndex = -1
        }
    }

    fun tab(index: Int) = if (activity.addMode() == "NORMAL") normalTabs[index] else incognitoTabs[index]

    fun currentTab() = tab(currentIndex)

    fun webView(index: Int) = if (activity.addMode() == "NORMAL") normalTabs[index].web_view else incognitoTabs[index].web_view

    fun currentWebView() = webView(currentIndex)

    fun changeMode() = activity.addMode()

    fun loadUrl(webView: WebView) = if (activity.addMode() == "NORMAL")  webView.url else webView.url

    fun cleanUrl() = if (activity.addMode() == "NORMAL")  activity.address_bar.setText("") else activity.address_bar.setText("")

    fun setAppBarLayout() = if (activity.addMode() == "NORMAL"){
                                activity.supportActionBar?.setBackgroundDrawable(
                                ColorDrawable(Color.GREEN))

                            }else{
                                activity.supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.GRAY))
                            }

    fun setRegisterContextMenu() = activity.registerForContextMenu(currentWebView())
}