package org.midorinext.android.tabs

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import org.midorinext.android.R

class CountDrawable(context: Context): Drawable() {

    private var badgePaint: Paint? = null
    private var textPaint: Paint? = null
    private var textRect: Rect? = null

    private var mCount: String = ""
    private var willDraw: Boolean = true

    init {
        val textSize: Float = context.resources.getDimension(R.dimen.badge_count_textsize)
        textRect = Rect()
        badgePaint = Paint()
        badgePaint?.setColor(ContextCompat.getColor(context.applicationContext, R.color.actionBarBackground))
        badgePaint?.isAntiAlias = true
        badgePaint?.style  = Paint.Style.FILL

        textPaint = Paint()
        textPaint!!.setColor(Color.WHITE)
        textPaint!!.setTypeface(Typeface.DEFAULT)
        textPaint!!.textSize = textSize
        textPaint!!.isAntiAlias = true
        textPaint!!.textAlign = Paint.Align.CENTER
    }


    override fun draw(canvas: Canvas) {
        if (!willDraw) return

        val bounds: Rect = bounds
        val with = bounds.right - bounds.left
        val height = bounds.bottom - bounds.top

        with.toFloat()
        height.toFloat()

        val radius = ((Math.max(with, height) / 2)) / 2
        val centerX = (with -radius -1) + 5
        val centerY = radius -5

        if (TabInfo.count().toString().length <= 2){
            canvas.drawCircle(centerX.toFloat(), centerY.toFloat(), (radius+5.5).toFloat(), badgePaint!!)
        }else{
            canvas.drawCircle(centerX.toFloat(), centerY.toFloat(), (radius+6.5).toFloat(), badgePaint!!)
        }

        textPaint?.getTextBounds(TabInfo.count().toString(), 0, mCount.length, textRect )
        val mTextHeight = (textRect!!.bottom - textRect!!.top)
        val textY = centerY + (mTextHeight.toFloat() / 2F)
        if (TabInfo.count().toString().length > 2 ){
            canvas.drawText("99+", centerX.toFloat(), textY, textPaint!!)
        }else canvas.drawText(TabInfo.count().toString(), centerX.toFloat(), textY, textPaint!!)
    }

    fun setCount(count: String){
        mCount = count

        willDraw = !count.equals("0")
        invalidateSelf()
    }

    override fun setAlpha(alpha: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSPARENT
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}