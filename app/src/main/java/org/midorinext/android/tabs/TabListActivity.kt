package org.midorinext.android.tabs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import org.midorinext.android.R
import kotlinx.android.synthetic.main.activity_tab_list.*

class TabListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_list)

        setSupportActionBar(toolbar)
        if (TabInfo.changeMode() == "NORMAL"){
            supportActionBar?.title = "Tabs"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#4da80d")))
            toolbar.setTitleTextColor(Color.parseColor("#000000"))

            val linearLayoutManager = LinearLayoutManager(this)
            val tabAdapter = TabAdapter()
            tabAdapter.activityList = this
            recycler_view.apply {
                adapter = tabAdapter
                layoutManager = linearLayoutManager
            }
        }else{
            supportActionBar?.title = "Tabs Incognito"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.GRAY))
            toolbar.setTitleTextColor(Color.parseColor("#000000"))

            val linearLayoutManager = LinearLayoutManager(this)
            val tabAdapter = TabAdapter()
            tabAdapter.activityList = this
            recycler_view.apply {
                adapter = tabAdapter
                layoutManager = linearLayoutManager
            }

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.tab_list_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.add_tab -> {
                TabInfo.activity.addTab(TabInfo.changeMode())
                TabInfo.cleanUrl()
                finish()
                return true
            }
            R.id.close_all -> {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Close all tabs")
                builder.setPositiveButton("Yes") { _, _ ->
                    TabInfo.removeAllTabs()
                    finish()
                }
                builder.setNegativeButton("No") { _, _ -> }
                val dialog = builder.create()
                dialog.show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
