package org.midorinext.android

import androidx.room.Database
import androidx.room.RoomDatabase
import org.midorinext.android.bookmarks.Bookmark
import org.midorinext.android.bookmarks.BookmarkDao
import org.midorinext.android.history.History
import org.midorinext.android.history.HistoryDao

@Database(entities = [Bookmark::class, History::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun bookmarkDao(): BookmarkDao
    abstract fun historyDao(): HistoryDao
}