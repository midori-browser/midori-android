package org.midorinext.android.api

import android.util.Log
import java.net.URL

class Request(val url: String) {

    fun run(){

        val tiempoJsonStr = URL(url).readText()
        Log.d(javaClass.simpleName, tiempoJsonStr)
    }
}