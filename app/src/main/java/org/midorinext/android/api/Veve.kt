package org.midorinext.android.api

import android.webkit.JavascriptInterface

data class Veve(var rank: String,var brand: String,var category: String,var rurl:String, var iurl:String,var impurl:String, var cc:String) {

    @JavascriptInterface
    fun getUrl():String{
        return rurl
    }

    @JavascriptInterface
    fun getIcon(): String{
        return iurl
    }

    @JavascriptInterface
    fun getTitle(): String{
        return brand
    }

    @JavascriptInterface
    fun getRanking(): String{
        return rank
    }

    @JavascriptInterface
    fun getImpUrl(): String{
        return impurl
    }
}