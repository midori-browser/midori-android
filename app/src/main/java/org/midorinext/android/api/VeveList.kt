package org.midorinext.android.api

import com.google.gson.annotations.SerializedName

data class VeveList(@SerializedName("data") var data: List<Veve>) {
}