package org.midorinext.android.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface VeveApiService {

    @GET("qlapi?o=unh85&s=92433&is=96x96&n=6")
    fun getAllUrl(): Call<VeveList>

    @GET("qlapi?o=unh85&s=92433&is=96x96&n=6")
    fun getAllUrl(@Query("di") id:String, @Query("cc") countryCode:String): Call<VeveList>

}
