package org.midorinext.android

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.widget.ProgressBar
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.view_tab.view.*
import org.midorinext.android.tabs.TabInfo


class WebView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : NestedWebView(context, attrs, defStyleAttr) {

    lateinit var activity: TabActivity
    var progressBar: ProgressBar? = null

    init {
        webViewClient = WebViewClient()
        webChromeClient = WebChromeClient()

        initSettings()
    }

    fun loadHome() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val address = preferences.getString("homepage", "midori://home")
        web_view.loadUrl(address)
    }

    fun search(query: String) {
        web_view.loadUrl(magicUri(query) ?: uriForSearch(query))
    }

    @SuppressLint("SetJavaScriptEnabled")

    fun initSettings() {

        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val userAgentList = settings.userAgentString.split(" ").toMutableList()
        userAgentList.add(userAgentList.size - 1, "Midori Browser/1.2")
        val defaultUserAgent = userAgentList.joinToString (separator = " ") { it -> it }
        var prefUserAgent = preferences.getString("useragent", defaultUserAgent)
        if (prefUserAgent.isNullOrBlank()) prefUserAgent = defaultUserAgent

        settings.apply {
            javaScriptEnabled = preferences.getBoolean("javascript", true)
            blockNetworkImage = !preferences.getBoolean("load_image", true)
            useWideViewPort = preferences.getBoolean("viewport", true)
            saveFormData = preferences.getBoolean("formdata", true)
            userAgentString = prefUserAgent
            setGeolocationEnabled(preferences.getBoolean("location", true))
            isLongClickable = true
            domStorageEnabled = preferences.getBoolean("domstorage", true)
            isClickable = true
            databaseEnabled = true
            mediaPlaybackRequiresUserGesture = true
            loadWithOverviewMode = true
        }
        CookieManager.getInstance().setAcceptCookie(preferences.getBoolean("cookies", true))
        CookieManager.getInstance().setAcceptThirdPartyCookies(this, preferences.getBoolean("3rd_party_cookies", true))
    }

    fun magicUri(text: String): String? {
        if (" " in text) {
            return null
        } else if (text.startsWith("https")) {
            return text
        } else if (text == "localhost" || "." in text) {
            return "http://" + text
        }
        return null
    }

    val locationEntrySearch = TabInfo.activity.searchEngine() as String
    fun uriForSearch(keywords: String? = null, search: String? = null): String{
        val uri = search ?: locationEntrySearch
        val escaped = Uri.encode(keywords ?: "", ":/")
        // Allow DuckDuckGo to distinguish Midori and in turn share revenue
        if (uri == "https://duckduckgo.com/?q=%s") {
            return "https://duckduckgo.com/?q=$escaped&t=midori"
        } else if ("%s" in uri) {
            return uri.format(escaped)
        }
        return uri + escaped
    }
}
