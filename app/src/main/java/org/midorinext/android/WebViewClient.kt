package org.midorinext.android

import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.os.AsyncTask
import android.webkit.CookieManager
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import androidx.core.view.isVisible
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_tab.*
import org.midorinext.android.adblock.Adblocker
import org.midorinext.android.history.History
import org.midorinext.android.tabs.TabInfo
import java.io.ByteArrayInputStream
import java.net.HttpURLConnection
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URL

class WebViewClient : android.webkit.WebViewClient() {
    var preferences: SharedPreferences? = null

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)

        val webView = view as org.midorinext.android.WebView
        TabInfo.activity.address_bar.setText(webView.url.toString())
        webView.progressBar?.isVisible = true
        webView.progressBar?.progressDrawable?.setColorFilter(Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN)
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)

        val webView = view as org.midorinext.android.WebView
        webView.progressBar?.isVisible = false



        if (TabInfo.changeMode() == "NORMAL") {
            val history = History(null, webView.title, webView.url, System.currentTimeMillis())
            AsyncTask.execute {
                Database.db?.historyDao()?.insert(history)
            }
        }
    }

    override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {

	        // Handle midori scheme
        if (request?.url?.scheme.equals("midori") && view != null) {
            val host = request?.url?.host
            try {
                val data = view.context.assets.open("web/$host.html").bufferedReader().readText()
                return WebResourceResponse("text/html", "utf-8", ByteArrayInputStream(data.toByteArray()))
            } catch (e: java.lang.Exception) {

            }
        }	

        if (preferences == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(view?.context)
        }

        val uri = request?.url
        if (preferences!!.getBoolean("adblock", true) && Adblocker.contains(uri)) {
            return Adblocker.emptyResponse()
        }

        if (preferences!!.getBoolean("proxy", false)) {
            try {
                val url = URL(uri.toString())
                val host = preferences!!.getString("proxy_host", "localhost")
                val port = preferences!!.getInt("proxy_port", 8000)

                val proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress(host, port))
                HttpURLConnection.setFollowRedirects(true)
                val connection = url.openConnection(proxy) as HttpURLConnection
                connection.instanceFollowRedirects = true

                val cookies = CookieManager.getInstance().getCookie(uri.toString())
                connection.addRequestProperty("Cookie", cookies)
                connection.addRequestProperty("User-Agent", view?.settings?.userAgentString)

                val type = connection.contentType ?: "text/plain"
                val encoding = connection.contentEncoding ?: "utf-8"
                val data = connection.inputStream.bufferedReader().readText()

                return WebResourceResponse(type, encoding, ByteArrayInputStream(data.toByteArray()))
            } catch (e: Exception) {
                return WebResourceResponse("text/plain", "utf-8", ByteArrayInputStream("Proxy Error".toByteArray()))
            }

        }
        return super.shouldInterceptRequest(view, request)
    }
}
