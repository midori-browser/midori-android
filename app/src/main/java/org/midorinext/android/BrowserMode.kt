package org.midorinext.android

enum class BrowserMode {
    Normal,
    Incognito
}
