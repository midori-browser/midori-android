package org.midorinext.android

import android.app.Activity
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Rect
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.URLUtil
import android.webkit.WebView
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import org.midorinext.android.adblock.Adblocker
import org.midorinext.android.bookmarks.Bookmark
import org.midorinext.android.bookmarks.BookmarkActivity
import org.midorinext.android.history.HistoryActivity
import org.midorinext.android.tabs.Tab
import org.midorinext.android.tabs.TabInfo
import org.midorinext.android.tabs.TabListActivity
import kotlinx.android.synthetic.main.activity_tab.*
import kotlinx.android.synthetic.main.view_tab.view.*
import org.midorinext.android.api.*
import org.midorinext.android.tabs.CountDrawable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.webkit.JavascriptInterface
import android.widget.AdapterView
import java.util.*

class TabActivity : AppCompatActivity() {

    var forwardButton: MenuItem? = null
    var countButton: MenuItem? = null
    var mMode: String? = "NORMAL"
    var isTrue: Boolean? = true
    private val CONTEXT_MENU_ID_DOWNLOAD_IMAGE = 1
    private val OPEN_NEW_TAB_IMAGE = 2
    private val OPEN_NEW_INC_TAB_IMAGE = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Database.initDb(this)
        Adblocker.init(this)
        TabInfo.activity = this


        setContentView(R.layout.activity_tab)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        setSupportActionBar(toolbar)
        address_bar.setOnFocusChangeListener { view, b ->
            run {
                if (!view.hasFocus()) {
                    hideKeyboard(view)
                }
            }
        }

        address_bar.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                view.clearFocus()
                val query = view.text.toString()
                TabInfo.currentWebView().search(query)
                true
            } else {
                false
            }
        }
        address_bar.setupClearButtonWithAction()
        onDownloadComplete()
        getVeve()
    }

    override fun onResume() {
        super.onResume()
        refreshTabs()
        TabInfo.setRegisterContextMenu()
        getVeve()
    }

    override fun onRestart() {
        super.onRestart()
        reloadCurrentTab()
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun reloadCurrentTab() {
        val webView = TabInfo.currentWebView()
        webView.reload()
    }

    fun addTab(mMode: String?) {
        val tab = Tab(this)
        tab.web_view.loadHome()
        TabInfo.addTab(tab, mMode)

        refreshTabs()
    }

    private fun refreshTabs() {
        if (TabInfo.currentIndex == -1) {
            addTab(mMode)
        } else {
            val tab = TabInfo.currentTab()
            frame_layout.removeAllViews()
            frame_layout.addView(tab)

            countButton?.title = TabInfo.count().toString()

            refreshForwardButton()
        }
    }

    private fun refreshForwardButton() {
        if (TabInfo.currentWebView().canGoForward()) {
            forwardButton?.isEnabled = true
            forwardButton?.icon?.alpha = 255
        } else {
            forwardButton?.isEnabled = false
            forwardButton?.icon?.alpha = 25
        }
    }

    override fun onBackPressed() {
        val webView = TabInfo.currentWebView()
        if (webView.canGoBack()) {
            webView.goBack()
            refreshForwardButton()
        } else super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.tab_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        forwardButton = menu?.findItem(R.id.forward_button)
        refreshForwardButton()
        if (TabInfo.changeMode() == "NORMAL") {
            menu?.getItem(4)!!.isVisible = false
        } else {
            menu?.getItem(3)!!.isVisible = false
            menu.getItem(4).isVisible = true
            menu.getItem(5).isVisible = false
        }
        countButton = menu.findItem(R.id.tab_list_button)
        val icon = countButton?.icon as LayerDrawable
        setCount(this, icon, TabInfo.count().toString())
        return super.onPrepareOptionsMenu(menu)
    }

    fun setCount(context: Context, icon: LayerDrawable, count: String) {

        var badge = CountDrawable(context)
        val reuse = icon.findDrawableByLayerId(R.id.ic_group_count)
        if (reuse != null && reuse is CountDrawable) {
            badge = reuse
        } else {
            badge = CountDrawable(context)
        }

        badge.setCount(count)
        icon.mutate()
        icon.setDrawableByLayerId(R.id.ic_group_count, badge)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.forward_button -> {
                TabInfo.currentWebView().goForward()
                refreshForwardButton()
            }
            R.id.reload_button -> {
                reloadCurrentTab()
                return true
            }
            R.id.tab_list_button -> {
                val intent = Intent(this, TabListActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.open_new_tab -> {
                isTrue = true
                mMode = "NORMAL"
                addTab(mMode)
                TabInfo.setAppBarLayout()
                return true
            }
            R.id.new_incognito_tab -> {
                isTrue = false
                mMode = "INCOGNITO"
                if (mMode == "INCOGNITO") {
                    addMode()
                }
                addTab(mMode)
                TabInfo.setAppBarLayout()
                return true
            }
            R.id.close_incognito_tab -> {
                val intent = Intent(this, TabActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.add_bookmark -> {
                val webView = TabInfo.currentWebView()
                val bookmark = Bookmark(webView.title, webView.url)
                AsyncTask.execute {
                    Database.db?.bookmarkDao()?.insert(bookmark)
                }
            }
            R.id.show_bookmarks -> {
                val intent = Intent(this, BookmarkActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.show_history -> {
                val intent = Intent(this, HistoryActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.download -> {
                val intent = Intent()
                intent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS)
                startActivity(intent)
            }
            R.id.settings_item -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.about_item -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev?.action == MotionEvent.ACTION_DOWN) {
            val view = currentFocus
            if (view is AutoCompleteTextView) {
                val rect = Rect()
                view.getGlobalVisibleRect(rect)
                if (!rect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                    view.clearFocus()
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    fun addMode(): String? {
        if (isTrue != false) {
            return "NORMAL"
        } else {
            return "INCOGNITO"
        }
    }

    fun AutoCompleteTextView.setupClearButtonWithAction() {

        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                val clearIcon =
                    if (editable?.isNotEmpty() == true) R.drawable.ic_close_black_24dp else 0
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, clearIcon, 0)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) =
                Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })

        setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= (this.right - this.compoundPaddingRight)) {
                    this.setText("")
                    return@OnTouchListener true
                }
            }
            return@OnTouchListener false
        })
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val result = TabInfo.currentWebView().hitTestResult
        val urlOriginal: String = TabInfo.currentWebView().originalUrl.toString()
        val title: String = TabInfo.currentWebView().title.toString()
        menu?.setHeaderTitle("Download")
        menu?.setHeaderTitle(urlOriginal)
        TabInfo.currentWebView().hitTestResult?.let {
            when (it.type) {
                WebView.HitTestResult.IMAGE_TYPE,
                WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE -> {
                    menu?.add(0, CONTEXT_MENU_ID_DOWNLOAD_IMAGE, 0, R.string.download_image)
                        ?.setOnMenuItemClickListener {
                            val imgUrl = result.extra
                            if (URLUtil.isNetworkUrl(imgUrl) && URLUtil.isValidUrl(imgUrl)) {
                                val request: DownloadManager.Request =
                                    DownloadManager.Request(Uri.parse(imgUrl))
                                request.allowScanningByMediaScanner()
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                                val downloadManager =
                                    getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                                downloadManager.enqueue(request)
                            }
                            false
                        }
                    menu?.add(1, OPEN_NEW_TAB_IMAGE, 1, "Open New Tab")
                        ?.setOnMenuItemClickListener {
                            val url = result.extra
                            val pdf = result.type

                            if (URLUtil.isValidUrl(url) && URLUtil.isNetworkUrl(url)) {
                                TabInfo.addTab(url.toString(), "NORMAL")
                                refreshTabs()
                            }
                            false
                        }
                    menu?.add(2, OPEN_NEW_INC_TAB_IMAGE, 2, "Open New Incognito Tab")
                        ?.setOnMenuItemClickListener {
                            val url = result.extra
                            if (URLUtil.isValidUrl(url) && URLUtil.isNetworkUrl(url)) {
                                TabInfo.addTab(url.toString(), "INCOGNITO")
                                refreshTabs()
                            }
                            false
                        }
                }
                else -> Toast.makeText(
                    this,
                    "It is not a type of image allowed", Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun onDownloadComplete() {

        val onComplete = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Toast.makeText(context, "File Downloaded", Toast.LENGTH_SHORT).show()
            }
        }
        registerReceiver(onComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
    }

    fun searchEngine(): String? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val address = preferences.getString("search_engine", "https://duckduckgo.com/?q=%s")
        return address
    }

    fun getVeve() {
        val androidId: String =
            Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
        val location = Locale.getDefault().country
        val service = RetrofitClientInstance.retrofitInstance?.create(VeveApiService::class.java)
        val call = service?.getAllUrl(androidId, location)

        call?.enqueue(object : Callback<VeveList> {

            override fun onResponse(call: Call<VeveList>, response: Response<VeveList>) {
                val body = response.body()
                val data = body!!.data
                val act = TabInfo.currentWebView()

                val url0 = data[0]
                val url1 = data[1]
                val url2 = data[2]
                val url3 = data[3]
                val url4 = data[4]
                val url5 = data[5]


                url0.getUrl()
                url0.getIcon()
                url0.getTitle()
                url0.getRanking()
                url0.getImpUrl()

                url1.getUrl()
                url1.getIcon()
                url1.getTitle()
                url1.getImpUrl()

                url2.getUrl()
                url2.getIcon()
                url2.getTitle()
                url2.getImpUrl()

                url3.getUrl()
                url3.getIcon()
                url3.getTitle()
                url3.getImpUrl()

                url4.getUrl()
                url4.getIcon()
                url4.getTitle()
                url4.getImpUrl()

                url5.getUrl()
                url5.getIcon()
                url5.getTitle()
                url5.getImpUrl()

                act.addJavascriptInterface(url0, "Veve0")
                act.addJavascriptInterface(url1, "Veve1")
                act.addJavascriptInterface(url2, "Veve2")
                act.addJavascriptInterface(url3, "Veve3")
                act.addJavascriptInterface(url4, "Veve4")
                act.addJavascriptInterface(url5, "Veve5")
            }

            override fun onFailure(call: Call<VeveList>, t: Throwable) {
                Toast.makeText(applicationContext, "Error reading JSON", Toast.LENGTH_LONG).show()
            }
        })
    }
}
